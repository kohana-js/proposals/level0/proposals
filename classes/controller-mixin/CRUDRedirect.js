const pluralize = require('pluralize');
const {KohanaJS, ControllerMixin} = require('kohanajs');

class ControllerMixinCRUDRedirect extends ControllerMixin{
  constructor(client, pathPrefix='admin/') {
    super(client);
    this.pathPrefix = pathPrefix;
  }

  entitySupport(){
    const request = this.client.request;
    const tpl = this.client.tpl;

    const entity = request.params.entity;
    if(!entity)return;
    const entityID = request.params.entityID;

    const entityClass = KohanaJS.require(ORM.prepend(pluralize.singular(entity)));
    if(!entityClass)throw new Error('invalid entity');

    const destination = (!!entity) ? `/${this.pathPrefix}${entity}/${entityID}` : null;

    Object.assign(
      tpl.data,
      {destination: tpl.data.destination || destination, entityClass: entityClass, entity: entity, entityID: entityID}
    )
  }

  async action_read() {
    this.entitySupport();
  }

  async action_create(){
    this.entitySupport();
  }

  async action_update(){
    this.redirectAfterFormSubmit(`/${this.pathPrefix}${this.client.model.tableName}/${this.client.mixin.get('id')}`);
  }

  async action_multiple_update(){
    this.redirectAfterFormSubmit(`/${this.pathPrefix}${this.client.model.tableName}`);
  }

  redirectAfterFormSubmit(defaultDestination){
    const $_POST      = this.client.mixin.get('$_POST');
    const destination = (!$_POST['destination'] || $_POST['destination'] === '')? defaultDestination : $_POST['destination'];
    this.client.redirect(destination);
  }

  async action_delete(){
    if(!this.client.request.query['confirm'])return;

    const checkpoint = (this.client.mixin.get('$_GET').cp);
    this.redirectAfterFormSubmit(checkpoint ? checkpoint : `/${this.pathPrefix}${this.client.model.tableName}`);
  }

}
module.exports = ControllerMixinCRUDRedirect;