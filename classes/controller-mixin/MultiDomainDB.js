const path = require('path');
const {KohanaJS, ControllerMixin} = require('kohanajs');
const fs = require('fs');
const Database = require('better-sqlite3');
const url = require('url');
const DB = {pool : []};

class MultiDomainDB extends ControllerMixin{
  constructor(client, databaseNames=["session"]) {
    super(client);

    const requestURL = url.parse('https://' + this.request.hostname);
    const hostname = requestURL.hostname || 'localhost';

    //guard hostname not registered.
    if(!fs.existsSync(path.normalize(`${KohanaJS.EXE_PATH}/../sites/${hostname}`))){
      this.client.notFound(`404 / store ${hostname} not registered`);
      return;
    }

    const conn = MultiDomainDB.getConnections(hostname, databaseNames);
    this.addBehaviour('hostname', hostname);
    this.addBehaviour('databases', conn);
    this.addBehaviour('db', conn.content);
  }


  static getConnections(hostname, databaseNames){
    if(!KohanaJS.config.cache.database || !DB.pool[hostname]){
      const dbPath = path.normalize(`${KohanaJS.APP_PATH}/../../sites/${hostname}/db`);

      DB.pool[hostname] = {
        access_at   : Date.now()
      };

      databaseNames.forEach(name=>{
        DB.pool[hostname][name] = new Database(dbPath + '/' + name + '.sqlite');
      });
    }

    const connection = DB.pool[hostname];
    connection.access_at = Date.now();
    return connection;
  }
}

module.exports = MultiDomainDB;