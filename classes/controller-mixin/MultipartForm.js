const {ControllerMixin, KohanaJS} = require('kohanajs');
const qs = require('qs');
const HelperForm = KohanaJS.require('helper/Form');

class MultipartForm extends ControllerMixin{
  constructor(client) {
    super(client);
    this.addBehaviour('$_GET', this.request.query || {});
  }
  async before(){
    //no request body
    if( !this.request.body && !this.request.multipart)return;
    const postData = (typeof this.request.body === 'object') ?
      Object.assign({}, this.request.body) :
      qs.parse(this.request.body);

    if(this.client.request.headers['content-type'] === 'multipart/form-data'){
      await HelperForm.parseMultipartForm(this.request, postData);
    }

    this.addBehaviour('$_POST', postData);
  }
}

module.exports = MultipartForm;