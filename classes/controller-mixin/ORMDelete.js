const {ControllerMixin} = require('kohanajs');

class ControllerMixinORMDelete extends ControllerMixin{
  async action_delete(){
    const model = this.client.model;
    const id = this.client.mixin.get('id');

    if(!id){
      throw new Error(`500 / Delete ${model.name} require object id`);
    }

    //do nothing without ?confirm in url
    if(!this.client.request.query['confirm'])return;

    const instance = new model(id, {database: this.client.mixin.get('db')});
    await instance.delete();
  }

}
module.exports = ControllerMixinORMDelete;