const {KohanaJS, ControllerMixin, ORM} = require('kohanajs');
class ControllerMixinProxyTable extends ControllerMixin{
  constructor(client, databases=[]) {
    super(client);
    this.databases = databases;
  }

  async action_update(){
    const Proxy = KohanaJS.require(ORM.prepend(`Proxy${this.client.model.name}`))
    const id = this.client.mixin.get('instance').id;

    await Promise.all(
      this.databases.map(async database =>{
        await ORM.create(Proxy, {database: database, createWithId: id}).save();
      })
    )
  }

  async action_delete(){
    if(!this.client.request.query['confirm'])return;

    const Proxy = KohanaJS.require(ORM.prepend(`Proxy${this.client.model.name}`))
    const id = this.client.mixin.get('id');

    await Promise.all(
      this.databases.map(async database =>{
        await new Proxy(id, {database: database}).delete();
      })
    )
  }
}

module.exports = ControllerMixinProxyTable;